#author: mau
#coding: utf-8
from panda3d.core import Vec2, LPoint2f, CollisionRay, CollisionNode, CollisionHandlerQueue, Vec3

from Entidades.Entidad_Juego import Entidad_Juego
from Entidades.Proyectil import Proyectil


class Jugador(Entidad_Juego):
    def __init__(self,nombre_textura,vida,velocidad,nombre_colicion):
        Entidad_Juego.__init__(self,nombre_textura,vida,velocidad,nombre_colicion)

        self.yVector = Vec2(0, 1)
        self.__lista_proyectiles = []

        self.mouse = Vec3(0,0,0)

        base.pusher.addCollider(self.colicion, self.entidad)
        base.cTrav.addCollider(self.colicion, base.pusher)
        self.velocidad_disparo = 48
        self.tiempo_transcurrido = 0

    def asignar_proyectil(self,objetivo):
        self.__lista_proyectiles.append(objetivo)

    def angulo_jugador(self,mouse):
        if isinstance(mouse,LPoint2f):
            self.insertar_posiciones_mouse(mouse)
            proceso = self.yVector.signedAngleDeg(Vec2(mouse.x,mouse.y).normalized())
            self.entidad.setH(proceso)

    def insertar_posiciones_mouse(self,valor):
        copiado_vect = valor.normalized()
        self.mouse = Vec3(copiado_vect.x,copiado_vect.y,0)


    def actualizar(self,teclado,dt):
        Entidad_Juego.actualizar(self,dt)
        self.cantidad_proyectil_actual = len(self.__lista_proyectiles)
        self.tiempo_transcurrido += 1
        self.movimiento = False
        for proyectil in self.__lista_proyectiles:
            proyectil.actualizar(dt)
            if not proyectil.estado:
                self.__lista_proyectiles.remove(proyectil)
        if teclado.retornar_tecla("arriba"):
            self.movimiento = True
            self.velocidad.addY(self.aceleracion*dt)
        if teclado.retornar_tecla("abajo"):
            self.movimiento = True
            self.velocidad.addY(-self.aceleracion * dt)
        if teclado.retornar_tecla("derecha"):
            self.movimiento = True
            self.velocidad.addX(self.aceleracion * dt)
        if teclado.retornar_tecla("izquierda"):
            self.movimiento = True
            self.velocidad.addX(-self.aceleracion * dt)
        if teclado.retornar_tecla("disparar"):
            if (self.tiempo_transcurrido%50) >= self.velocidad_disparo:
                self.asignar_proyectil(Proyectil(self.entidad.getPos(), self.mouse.normalized()))

