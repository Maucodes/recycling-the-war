#coding: utf-8
#author: mau
from panda3d.core import TransparencyAttrib, CollisionNode, CollisionSphere, Vec3
FRICCION = 150.0

class Entidad_Juego():

    def __init__(self,textura,
                 max_vida,
                 max_velocidad,
                 nombre_coliccion,
                 pos = Vec3(.0,.0,.0)):

        self.entidad = self.cargar_objeto(base.loader.loadModel("modelos/plane"), textura, transparencia = True)
        self.entidad.setPos(pos)
        self.max_vida = max_vida
        self.vida = max_vida
        self.movimiento = False
        self.entidad.setScale(4)

        self.max_velocidad = max_velocidad
        self.velocidad = Vec3(0,0,0)
        self.aceleracion = 300.0

        nodo_colicion = CollisionNode(nombre_coliccion)
        nodo_colicion.addSolid(CollisionSphere(0, 0, 0, .3))
        self.colicion = self.entidad.attachNewNode(nodo_colicion)
        self.colicion.setPythonTag("owner", self)
        #self.colicion.show()

        self.entidad.reparentTo(base.render)

    def actualizar(self,dt):
        velocidad = self.velocidad.length()
        if velocidad > self.max_velocidad:
            self.velocidad.normalize()
            self.velocidad *= self.max_velocidad
            velocidad = self.max_velocidad
        if not self.movimiento:
            frictionVal = FRICCION * dt
            if frictionVal > velocidad:
                self.velocidad.set(0, 0, 0)
            else:
                frictionVec = -self.velocidad
                frictionVec.normalize()
                frictionVec *= frictionVal

                self.velocidad += frictionVec
        self.entidad.setPos(self.entidad.getPos() + self.velocidad*dt)
        self.entidad.setZ(0)

    def alterar_vida(self,dvida):
        self.vida += dvida
        if self.vida > self.max_vida:
            self.vida = self.max_vida

    def getPos(self):
        return self.entidad.getPos()

    def reiniciar(self):
        #eliminando nodos
        if self.colicion is not None and not self.colicion.isEmpty():
            self.colicion.clearPythonTag("owner")
            base.cTrav.removeCollider(self.colicion)
            base.pusher.removeCollideddddr(self.colicion)

        if self.entidad is not None:
            self.entidad.removed()
            self.entidad.removeNode()
            self.entidad = None
        self.colicion = None

    def cargar_objeto(self,modelo,ruta_textura = None,transparencia = False):
        texturas = ["texturas/",".png"]
        objeto = modelo
        objeto.setP(-90)
        if ruta_textura is None:
            transparencia = False
        else:
            text = base.loader.loadTexture(texturas[0]+ruta_textura+texturas[1])
            objeto.setTexture(text)

        if transparencia:
            objeto.setTransparency(TransparencyAttrib.MAlpha)
            objeto.setTexture(text)
        return objeto