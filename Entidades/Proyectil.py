#coding: utf-8
#author: mau
from panda3d.core import TransparencyAttrib, Vec3


class Proyectil():
    def __init__(self,pos_entidad,pos_direccion):
        self.entidad = self.cargar_objeto(base.loader.loadModel("modelos/plane"),
                                          "proyectil1",
                                          transparencia = True)
        self.entidad.setScale(4)
        self.animacion = 0
        self.pos_entidad = pos_entidad
        self.pos_direccion = pos_direccion
        self.entidad.setPos(self.pos_entidad)
        self.entidad.setX(self.entidad.getX()+.2)
        self.entidad.setY(self.entidad.getY() + .2)
        self.distancia_proyectil = 400.0
        self.estado = True
        self.entidad.reparentTo(base.render)

    def eliminar_nodo(self):
        self.entidad.removeNode()

    def cargar_objeto(self,modelo,ruta_textura = None,transparencia = False):
        texturas = ["texturas/",".png"]
        objeto = modelo
        objeto.setP(-90)
        if ruta_textura is None:
            transparencia = False
        else:
            text = base.loader.loadTexture(texturas[0]+ruta_textura+texturas[1])
            objeto.setTexture(text)

        if transparencia:
            objeto.setTransparency(TransparencyAttrib.MAlpha)
            objeto.setTexture(text)
        return objeto

    def actualizar(self,dt):
        self.animacion += 1
        self.entidad.setH(self.animacion)
        self.entidad.setPos(self.entidad.getPos()+self.pos_direccion)
        distancia_proyectil = (self.entidad.getPos() - self.pos_entidad).length()
        if self.distancia_proyectil <= distancia_proyectil:
            self.estado = False
            self.eliminar_nodo()





