# coding: utf-8
# author: mau
from panda3d.core import Vec2

from Entidades.Enemigo import Enemigo


class MovimientoEnemigo(Enemigo):

    def __init__(self,nombre_textura,vida,velocidad,nombre_colicion):
        Enemigo.__init__(self,nombre_textura,vida,velocidad,nombre_colicion)

        self.distancia_ataque = 0.75
        self.aceleracion = 100.0

        # un vector de referencia utilizando el enfrentador de jugador direccion y ege
        self.yVector = Vec2(0, 1)

    def logica(self, jugador, dt):
        vectorjugador = jugador.entidad.getPos() - self.entidad.getPos()
        vectorJugador2D = vectorjugador.getXy()
        distanciaDelJugador = vectorJugador2D.length()

        vectorJugador2D.normalize()
        proceso = self.yVector.signedAngleDeg(vectorJugador2D)

        if distanciaDelJugador > self.distancia_ataque * 14:
            self.movimiento = True
            vectorjugador.setZ(0)
            vectorjugador.normalize()
            self.velocidad += vectorjugador * self.aceleracion * dt
        else:
            self.movimiento = False
            self.velocidad.set(0, 0, 0)
        self.entidad.setH(proceso)
