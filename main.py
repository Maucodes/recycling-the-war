#coding: utf-8
#author: mau
from direct.showbase.ShowBase import ShowBase
from direct.showbase.ShowBaseGlobal import globalClock
from panda3d.core import TransparencyAttrib, Vec3, CollisionHandlerPusher

from Entidades.MovimientoEnemigo import MovimientoEnemigo
from Entidades.Proyectil import Proyectil
from core.cargador_objetos import Cargador_Objetos
from controles.Teclado import Teclado
from panda3d.core import Point3
from core.Iluminacion import Iluminacion
from panda3d.core import CollisionTraverser
from Entidades.Jugador import Jugador


class Juego(ShowBase):

    def __init__(self):
        ShowBase.__init__(self)
        self.teclado = Teclado()
        self.agregar_teclados()
        #self.iluminacion = Iluminacion()
        self.en_funcionamiento = self.taskMgr.add(self.actualizar,"bucle_principal")
        self.disable_mouse()

        self.cTrav = CollisionTraverser()
        self.pusher = CollisionHandlerPusher()

        self.jugador = Jugador("jugador1",100,10,"jugador")
        self.enemigo = MovimientoEnemigo("jugador2",100,7,"enemigo")
        self.mundo = self.loader.loadModel("modelos/mundo")
        self.mundo.reparentTo(self.render)
        self.mundo.setZ(-1)




    def ajuste_automatico_camara(self):
        camaraPos = self.jugador.getPos()
        camaraPos.setZ(45)
        self.camera.setPos(camaraPos)
        self.camera.setP(-90)


    def actualizar(self,task):
        dt = globalClock.getDt()
        self.ajuste_automatico_camara()
        self.jugador.actualizar(self.teclado,dt)
        self.enemigo.actualizar(self.jugador,dt)

        if self.mouseWatcherNode.hasMouse():
            self.jugador.angulo_jugador(self.mouseWatcherNode.getMouse())

        return task.cont



    def agregar_teclados(self):
        self.accept("w", self.teclado.asignar_key,["arriba",True])
        self.accept("s", self.teclado.asignar_key, ["abajo", True])
        self.accept("a", self.teclado.asignar_key, ["izquierda", True])
        self.accept("d", self.teclado.asignar_key, ["derecha", True])
        self.accept("mouse1",self.teclado.asignar_key,["disparar",True])

        self.accept("w-up", self.teclado.asignar_key, ["arriba", False])
        self.accept("s-up", self.teclado.asignar_key, ["abajo", False])
        self.accept("a-up", self.teclado.asignar_key, ["izquierda", False])
        self.accept("d-up", self.teclado.asignar_key, ["derecha", False])
        self.accept("mouse1-up", self.teclado.asignar_key, ["disparar", False])

if __name__ == "__main__":
    juego = Juego()
    juego.run()