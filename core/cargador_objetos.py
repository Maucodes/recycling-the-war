#coding: utf-8
#author: mau
from panda3d.core import TransparencyAttrib
import copy
#Clase de pruebas para revisar objetos en el escenario
class Cargador_Objetos():

    def __init__(self):
        self.__imagenes_predeterminadas = {}
        self.__modelo = base.loader.loadModel("modelos/plane")
        self.__lista_objetos = {}

    def cargar_objeto(self,ruta_textura = None,transparencia = False):

        texturas = ["texturas/",".png"]

        objeto = self.__modelo
        #objeto.setH(-90)

        if ruta_textura is None:
            transparencia = False
        else:
            text = base.loader.loadTexture(texturas[0]+ruta_textura+texturas[1])
            objeto.setTexture(text)

        if transparencia:
            objeto.setTransparency(TransparencyAttrib.MAlpha)
            objeto.setTexture(text)

        return objeto