#coding: utf-8
#author: mau
class Teclado():

    def __init__(self):
        self.condicion = True
        self.__teclado = {"arriba":False,
                          "abajo":False,
                          "derecha":False,
                          "izquierda":False,
                          "salir":False,
                          "aceptar":False,
                          "disparar":False}

    def retornar_tecla(self,key):
        return self.__teclado[key]

    def asignar_key(self,key,valor):
        if self.condicion:
            self.__teclado[key] =valor
        else:
            self.__reiniciar()


    def __reiniciar(self):
        for keys in self.__teclado.keys():
            self.__teclado[keys] = False


    def retornar_teclado(self):
        return self.teclado